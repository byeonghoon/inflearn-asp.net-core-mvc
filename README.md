# 인프런 - 해외 취업 ASP.NET Core MVC 구현

[![Version](https://img.shields.io/badge/version-2018.32.0-red.svg)](./CHANGELOG)
[![Version](https://img.shields.io/github/license/mashape/apistatus.svg)](./LICENSE)


## What is the Project

이 프로젝트는 `해외 취업 ASP.NET Core MVC` 강의를 보고 제작한 코드 저장소입니다.

<br />

## Status

Version 2018.32.0

이 프로젝트의 버전 이력은 [CHANGELOG](./CHANGELOG)에서 확인하실 수 있습니다.

<br />

## Building

### Clone a copy of the repository:

```bash
// current-directory
$ git clone git@gitlab.com:byeonghoon/inflearn-asp.net-core-mvc.git

// Change to the project directory: inflearn-asp.net-core-mvc
$ cd inflearn-asp.net-core-mvc
```


### Modify project environment file:
  
`appsettings.json` 수정하여 시스템에 알맞게 수정합니다.

```bash
// current-directory: {project}

$ vi appsettings.json
```

<br />

## Contributors

- byeonghoon, `byeonghooni@icloud.com`
