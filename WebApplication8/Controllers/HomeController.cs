﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication8.Data.Repositories;
using WebApplication8.Models;
using WebApplication8.ViewModels;

namespace WebApplication8.Controllers
{
  public class HomeController : Controller
  {
    private readonly ITeacherRepository _teacherRepository;
    private readonly IStudentRepository _studentRepository;
    
    public HomeController(ITeacherRepository teacherRepository,
                          IStudentRepository studentRepository)
    {
      _teacherRepository = teacherRepository;
      _studentRepository = studentRepository;
    }
    
    [Authorize]
    public IActionResult Index()
    {
      var teachers = _teacherRepository.GetAllTeachers();

      var viewModel = new StudentTeacherViewModel
      {
        Student = new Student(),
        Teachers = teachers
      };

      return View(viewModel);
    }
    
    [Authorize(Roles = "Admin")]
    public IActionResult Student()
    {
      var students = _studentRepository.GetAllStudents();
      
      var viewModel = new StudentTeacherViewModel
      {
        Student = new Student(),
        Students = students
      };

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Admin")]
    public IActionResult Student(StudentTeacherViewModel model)
    {
      if (ModelState.IsValid)
      {
        _studentRepository.AddStudent(model.Student);
        _studentRepository.Save();
        
        ModelState.Clear(); // 폼 입력 값들 초기화
      }
      else
      {
        // 에러를 보여준다. 
      }
      
      var students = _studentRepository.GetAllStudents();

      var viewModel = new StudentTeacherViewModel
      {
        Student = new Student(),
        Students = students
      };
      
      return View(viewModel);
    }

    public IActionResult Detail(int id)
    {
      var result = _studentRepository.GetStudent(id);

      return View(result);
    }

    public IActionResult Edit(int id)
    {
      var result = _studentRepository.GetStudent(id);

      return View(result);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Edit(Student student)
    {
      if (ModelState.IsValid)
      {
        _studentRepository.Edit(student);
        _studentRepository.Save();

        return RedirectToAction("Student");
      }

      return View(student);
    }

    public IActionResult Delete(int id)
    {
      var result = _studentRepository.GetStudent(id);

      if (result != null)
      {
        _studentRepository.Delete(result);
        _studentRepository.Save();
      }

      return RedirectToAction("Student");
    }
  }
}