﻿using System.Collections.Generic;
using WebApplication8.Models;

namespace WebApplication8.Data.Repositories
{
  public interface IStudentRepository
  {
    void AddStudent(Student student);

    IEnumerable<Student> GetAllStudents();

    Student GetStudent(int id);

    void Save();

    void Edit(Student student);
    
    void Delete(Student student);
  }
}