﻿using System.Collections.Generic;
using WebApplication8.Models;

namespace WebApplication8.Data.Repositories
{
  public interface ITeacherRepository
  {
    IEnumerable<Teacher> GetAllTeachers();

    Teacher GetTeacher(int id);
  }
}