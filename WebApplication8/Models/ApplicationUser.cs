﻿using Microsoft.AspNetCore.Identity;

namespace WebApplication8.Models
{
  public class ApplicationUser: IdentityUser
  {
    public string FullName { get; set; }
    public string Gender { get; set; }
  }
}