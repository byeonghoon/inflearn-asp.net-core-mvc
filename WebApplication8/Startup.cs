﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApplication8.Data;
using WebApplication8.Data.Repositories;
using WebApplication8.Models;

namespace WebApplication8
{
  public class Startup
  {
    private readonly IConfiguration _config;

    public Startup(IConfiguration config)
    {
      _config = config;
    }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<MyAppContext>(options =>
      {
        options.UseNpgsql(_config.GetConnectionString("MyAppConnection"));
      });

      services.AddIdentity<ApplicationUser, IdentityRole>(options =>
        {
        options.Password.RequireDigit = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireUppercase = false;
        options.Password.RequireLowercase = false;
          options.Password.RequiredLength = 6;
        })
        .AddEntityFrameworkStores<MyAppContext>();
      
      services.AddTransient<DbSeeder>();
      services.AddScoped<ITeacherRepository, TeacherRepository>();
      services.AddScoped<IStudentRepository, StudentRepository>();
      services.AddMvc();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbSeeder seeder)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseStaticFiles();

      app.UseAuthentication();
      
      app.UseMvc(routes => { routes.MapRoute(name: "default", template: "{controller=Account}/{action=Login}/{id?}"); });

      seeder.SeedDatabase().Wait();
    }
  }
}
