﻿using System.Collections.Generic;
using WebApplication8.Models;

namespace WebApplication8.ViewModels
{
  public class StudentTeacherViewModel
  {
    public Student Student { get; set; }
    public IEnumerable<Teacher> Teachers { get; set; }
    public IEnumerable<Student> Students { get; set; }
  }
}